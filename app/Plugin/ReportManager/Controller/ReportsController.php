<?php

App::uses('ReportManagerAppController', 'ReportManager.Controller');
App::uses('Croogo', 'Lib');

class ReportsController extends ReportManagerAppController {
    public $uses = array('ReportManager.Report', 'ReportManager.Informer', 'ReportManager.Finding', 'ReportManager.ElectricCooperative');

    public $paginate = array(
        'limit' => 10,
        'order' => array(
            'Report.id' => 'desc'
        )
    );


	 public $components = array(
		'DataTable.DataTable' => array(
		    'columns' => array( 
			'date_submitted' => 'Created',
			'Informer.code' => 'Reporter\'s Codename',
		        'code' => 'Code',
				'ElectricCooperative.abbreviation' => 'Electric Company',
		        'address' => 'Location',
		        'complain_description' => 'Details',
		        'Finding.code' => 'Finding',
		        'id' => false,  
		        
		    ),
				'triggerAction' => array('index'),
		),
	    );

	
	public $helpers = array(
				'DataTable.DataTable',
			    );


	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow(array('index', 'stats', 'ec_stats'));
	}

	

    public function index() {

		$electricCooperativesCondition = array();
		
		if($this->request->is( 'ajax' ) ) {
			if(isset($_GET['ecId']) && !empty($_GET['ecId'])){
				$electricCooperativesCondition = array('Report.utility_name_id' => $_GET['ecId']);
				$this->DataTable->settings['conditions'] = array_merge($electricCooperativesCondition);
			}			
			$this->DataTable->process();
		}
       	 
    }
	
	public function stats(){

		$totalReport = 0;
		$totalProcessingReport = 0;
		$totalResolvedReport = 0;
		$totalReward = 0;

		$totalReport = $this->Report->find('count');
		$this->set('total_report', $totalReport);

		$totalProcessingReport = $this->Report->find('count', array('conditions' => array('Report.complain_condition_id BETWEEN ? AND ?' => array(2,4))));
		$this->set('total_processing', $totalProcessingReport);

		$totalResolvedReport = $this->Report->find('count', array('conditions' => array('Report.complain_condition_id BETWEEN ? AND ?' => array(5,6))));
		$this->set('total_resolved', $totalResolvedReport);

		$this->loadModel('ReportManager.NegotiatedComputation');

		$negotiatedComputations = $this->NegotiatedComputation->find('all');

		foreach ($negotiatedComputations as $key => $negotiatedComputation) {

		
		@$NC_negotiated_amount = $negotiatedComputation['NegotiatedComputation']["total_amount_as_negotiated"];
		@$NC_Kwh_meter = $negotiatedComputation['NegotiatedComputation']["meter_price"];
		@$NC_recon_fee = $negotiatedComputation['NegotiatedComputation']["reconnection_fee"];
		@$NC_dp = $negotiatedComputation['NegotiatedComputation']["downpayment"];
		@$NC_monthly_payment = $negotiatedComputation['NegotiatedComputation']["monthly_payment"];
		@$NC_no_of_months = $negotiatedComputation['NegotiatedComputation']["no_of_months"];
		@$NC_payment_mode = $negotiatedComputation['NegotiatedComputation']["mode_of_payment"];
		
			if($NC_payment_mode == "Installment"){
	       		 $NC_negotiated_amount = $NC_dp + ($NC_monthly_payment * $NC_no_of_months);
			}
			
			$reward = ((($NC_negotiated_amount - $NC_Kwh_meter) - $NC_recon_fee)* 0.15);
			$gli_reward = ($reward * 3)/15;
			//$reward =  $reward - $gli_reward;
			
			$witholding = $reward * 0.02;
			$balance = $reward - $witholding;
			$evat = ($balance * 0.12)/1.12;		
			$tax_to_reporter = (($balance - $evat - $gli_reward) * 0.03)/1.03 ;
			@$total_reward 	= ($balance - $evat - $tax_to_reporter - $gli_reward);
			$totalReward += $total_reward;
		
		}

		$this->set('total_reward', $totalReward);

	}


	public function ec_stats(){
		$this->autoRender = false;
		if(isset($_GET['id']) && $this->request->is( 'ajax' )){
			$ecId = $_GET['id'];

			$totalReport = 0;
			$totalProcessingReport = 0;
			$totalResolvedReport = 0;
			$success = 0;
			$totalReward = 0;
			$tReward = 0;

			$totalReport = $this->Report->find('count', array('conditions' => array('Report.utility_name_id' => $ecId)));
			$this->set('total_report', $totalReport);

			$totalProcessingReport = $this->Report->find('count', array('conditions' => array('AND'=> array(
																											array('Report.complain_condition_id BETWEEN ? AND ?' => array(2,4)),
																											array('Report.utility_name_id' => $ecId)
																											)
																							  ) ));
			$this->set('total_processing', $totalProcessingReport);

			$totalResolvedReport = $this->Report->find('count', array('conditions' => array('AND'=> array(
																											array('Report.complain_condition_id BETWEEN ? AND ?' => array(5,6)),
																											array('Report.utility_name_id' => $ecId)
																											)
																							)));
			$this->set('total_resolved', $totalResolvedReport);

			//$this->loadModel('ReportManager.Reward');

			// $totalReward = $this->Reward->find('all', array('fields' => array('sum(Reward.reward_amount) AS atotal'), 
			// 											  'conditions'=>array('AND'=> array(
			// 											  									array('Reward.reward_status_id' => 5 ),
			// 																				array('Reward.utility_name_id' => $ecId)
			// 																				)
			// 											  					)
			// 												));
			// $tReward = $totalReward[0][0]['atotal'];
			$this->loadModel('ReportManager.NegotiatedComputation');
			$negotiatedComputations = $this->NegotiatedComputation->find('all');

			foreach ($negotiatedComputations as $key => $negotiatedComputation) {

			
				@$NC_negotiated_amount = $negotiatedComputation['NegotiatedComputation']["total_amount_as_negotiated"];
				@$NC_Kwh_meter = $negotiatedComputation['NegotiatedComputation']["meter_price"];
				@$NC_recon_fee = $negotiatedComputation['NegotiatedComputation']["reconnection_fee"];
				@$NC_dp = $negotiatedComputation['NegotiatedComputation']["downpayment"];
				@$NC_monthly_payment = $negotiatedComputation['NegotiatedComputation']["monthly_payment"];
				@$NC_no_of_months = $negotiatedComputation['NegotiatedComputation']["no_of_months"];
				@$NC_payment_mode = $negotiatedComputation['NegotiatedComputation']["mode_of_payment"];
				
					if($NC_payment_mode == "Installment"){
			       		 $NC_negotiated_amount = $NC_dp + ($NC_monthly_payment * $NC_no_of_months);
					}
					
					$reward = ((($NC_negotiated_amount - $NC_Kwh_meter) - $NC_recon_fee)* 0.15);
					$gli_reward = ($reward * 3)/15;
					//$reward =  $reward - $gli_reward;
					
					$witholding = $reward * 0.02;
					$balance = $reward - $witholding;
					$evat = ($balance * 0.12)/1.12;		
					$tax_to_reporter = (($balance - $evat - $gli_reward) * 0.03)/1.03 ;
					@$total_reward 	= ($balance - $evat - $tax_to_reporter - $gli_reward);
					$totalReward += $total_reward;
			
			}

			if($ecId == 20){
				$tReward = $totalReward;
			} 
			$this->set('total_reward', $totalReward); //$totalReward[0][0]['atotal']

			$this->loadModel('ElectricCooperative');
			$ecData = $this->ElectricCooperative->findById($ecId);
			$msg1 = $ecData['ElectricCooperative']['name'];
						
			$msg2 =	"<br>".$ecData['ElectricCooperative']['address']."<br>".
							$ecData['ElectricCooperative']['phone'];	
						
			$msg3 =	$totalReport;
			$msg4 =	$totalProcessingReport;
			$msg5 =	$totalResolvedReport;
			if($tReward > 0){
				$msg6 =	number_format($tReward, 2);
			} else {
				$msg6 =  0;
			}
			$msg8 = 15;
			if($ecData['ElectricCooperative']['lat_long'] == ""){
				$ecData['ElectricCooperative']['lat_long'] = "12.554564,122.867661";
				$msg8 = 5;			
			} 
			$msg7 =	explode(",", $ecData['ElectricCooperative']['lat_long']);

			echo json_encode(array($msg1, $msg2, $msg3, $msg4, $msg5, $msg6, $msg7[0], $msg7[1], $msg8));
		}
	}
    


}
