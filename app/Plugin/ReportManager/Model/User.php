<?php


class User extends AppModel {

	public $useDbConfig = 'system';
	public $useTable = 'informers';
	public $displayField = 'username';


	public $hasMany = array(
		'Report' => array(
			'className' => 'ReportManager.Report',
			'foreignKey' => 'informer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}


?>