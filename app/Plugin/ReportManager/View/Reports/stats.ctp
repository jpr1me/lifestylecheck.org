	
	<!-- <div class="panel panel-default well">
                <center><strong>Totals</strong></center>        
       <div class="panel-body">
                Number of reports <span class="badge pull-right"><?php echo $total_report; ?></span>
        </div>
		<div class="panel-body">
                Estimated amount <span class="badge pull-right"><?php echo 'P '.number_format($totalEstAmount, 2); ?></span>
        </div>
   	</div> -->

    <div class="stats"><strong>Reported</strong>   <span class="pull-right"><?php echo $total_report; ?></span></div>
    <div class="stats"><strong>Processing</strong>   <span class="pull-right"><?php echo $total_processing; ?></span></div>
    <div class="stats"><strong>Resolved</strong> <span class="pull-right"><?php echo $total_resolved; ?></span></div>
    <div class="stats"><strong>Total Rewards</strong> <span class="pull-right">P <?php echo number_format($total_reward, 2); ?></span></div>